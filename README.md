# Game of Life

Rails implementation of [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

## Try it

### [Live Version](https://rails-game-of-life.herokuapp.com)

sign up or login as default user:

> email: user@example.com
>
> password: password

enjoy!

## Development

[![Ruby](https://badgen.net/badge/icon/ruby?icon=ruby&label)](https://https://ruby-lang.org/) [![Ruby Style Guide](https://img.shields.io/badge/code_style-rubocop-brightgreen.svg)](https://github.com/rubocop-hq/rubocop)

- Ruby 2.6.6

- Rails 6.1.4.4

setup db and start the server:

```sh
rails db:setup

rails server
```

## Testing

run tests:

```sh
rails test
```

run linter:

```sh
rubocop
```
