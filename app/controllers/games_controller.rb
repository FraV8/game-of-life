class GamesController < ApplicationController
  before_action :authenticate_user!

  rescue_from ActionController::ParameterMissing do
    redirect_to games_new_url
  end

  layout 'game'

  def play
    upload = Upload.new file_params

    if upload.valid?
      @game = Game.new upload
    else
      redirect_to games_new_url, alert: upload.errors
    end
  end

  def random
    @game = Game.new random: true

    render :play
  end

  def start
    game = Game.new game_params
    game.next_generation

    html = ApplicationController.render partial: 'games/grid', locals: { game: game }
    render json: { game: game, grid: html }
  end

  private

  def file_params
    params.require(:file)
  end

  def game_params
    JSON.parse params.require(:game), object_class: OpenStruct
  end
end
