module ApplicationHelper
  def inside_layout(layout, &block)
    render layout: "layouts/#{layout}", inline: capture(&block)
  end
end
