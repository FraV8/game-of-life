class Game
  attr :generation, :grid, :cols, :rows, :over

  MAX = 50

  def initialize(params)
    if params.try :[], :random
      randomize
    else
      @generation = params.generation.to_i
      @rows = params.rows.to_i
      @rows = MAX if rows > MAX
      @cols = params.cols.to_i
      @cols = MAX if cols > MAX

      if params.try :grid
        @grid = params.grid
      else
        @grid = load_grid
        fill params.cells if params.try :cells
      end
    end
  end

  def randomize
    @generation = 0
    @rows = @cols = rand 10..50
    @grid = load_random_grid
  end

  def next_generation
    new_grid = load_grid
    grid.each_with_index do |row, y|
      row.each_with_index do |col, x|
        count = neighbors_count(y, x)
        new_grid[y][x] = begin
          if col.zero?
            count == 3 ? 1 : 0
          else
            count.in?([2, 3]) ? 1 : 0
          end
        end
      end
    end
    @over = true if new_grid == grid
    @grid = new_grid
    @generation += 1
  end

  private

  def load_grid
    Array.new(rows) { Array.new(cols, 0) }
  end

  def load_random_grid
    Array.new(rows) do
      Array.new(cols) do
        case rand(1..100)
        when 1..80 then 0
        else 1
        end
      end
    end
  end

  def fill(cells)
    cells.each_with_index do |row, y|
      row.each_with_index do |col, x|
        grid[y][x] = 1 if col == 1
      end
    end
  end

  def neighbors_count(y, x)
    neighbors(y, x).count { |col| col == 1 }
  end

  def neighbors(y, x)
    (-1..1).each_with_object [] do |py, values|
      (-1..1).each do |px|
        next if py.zero? && px.zero?

        i = y + py
        j = x + px
        i = 0 unless i < rows
        j = 0 unless j < cols
        values << grid[i][j]
      end
    end
  end
end
