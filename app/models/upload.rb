class Upload
  include ActiveModel::Validations

  attr :file, :generation, :rows, :cols, :cells

  def initialize(file)
    @file = file
    parse_file
  end

  def parse_file
    @cells = []
    File.open(file) do |f|
      f.each_with_index do |line, i|
        case i
        when 0
          @generation = line.match(/\d+:/).to_s.scan(/\d+/).first
        when 1
          @rows, @cols = line.scan(/\d+/)
        else
          row = line.scan(/./).map { |s| s == '*' ? 1 : 0 }
          @cells << row unless row.empty?
        end
      end
    end
  rescue StandardError
    nil
  end

  validates_presence_of :file
  validates_size_of :file, maximum: 1.megabyte, message: 'Must not exceed 1MB'
  validate do
    generation or errors.add :file, 'Missing generation number'
    (rows and cols) or errors.add :file, 'Missing grid size'
    cells.present? or errors.add :file, 'Missing population state'
  end
end
