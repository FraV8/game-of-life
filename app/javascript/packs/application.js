// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
import * as ActiveStorage from "@rails/activestorage";
import Rails from "@rails/ujs";
import Turbolinks from "turbolinks";

import "channels";

Rails.start();
Turbolinks.start();
ActiveStorage.start();

window.addEventListener("load", () => {
  const grid = document.querySelector("#game");

  if (grid) {
    let loop;
    const run = () => {
      Rails.fire(document.forms[0], "submit");
    };

    const start = document.querySelector("#start");
    start.addEventListener("click", (e) => {
      loop = setInterval(run, 200);
      e.target.hidden = true;
      stop.hidden = false;
    });

    const stop = document.querySelector("#stop");
    stop.addEventListener("click", (e) => {
      clearInterval(loop);
      e.target.hidden = true;
      start.hidden = false;
    });

    document.querySelector("#new_game").addEventListener("click", () => {
      clearInterval(loop);
    });

    document.forms[0].addEventListener("ajax:success", (e) => {
      const [data] = e.detail;
      const { game } = data;
      grid.innerHTML = data.grid;
      document.querySelector("#game_json").value = JSON.stringify(game);
      if (game.over) {
        clearInterval(loop);
        stop.hidden = true;
        document.querySelector("#or").style.visibility = "hidden";
        document.querySelector("#gameover").style.display = "block";
      }
    });
  }
});
