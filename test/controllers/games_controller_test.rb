require 'test_helper'

class GamesControllerTest < ActionDispatch::IntegrationTest
  test 'should not get new w/o authentication' do
    get games_new_url
    assert_redirected_to new_user_session_path
  end

  test 'should get new w/ authenticated user' do
    sign_in users :one
    get games_new_url
    assert_response :ok
  end

  test 'should not post play w/o file' do
    sign_in users :one
    post games_play_url
    assert_redirected_to games_new_path
  end

  test 'should not post play w/ bad file' do
    sign_in users :one
    post games_play_url params: { file: file_fixture('bad_game.txt') }
    assert_redirected_to games_new_path
  end

  test 'should post play w/ good file' do
    sign_in users :one
    post games_play_url params: { file: file_fixture('good_game.txt') }
    assert_response :ok
  end

  test 'play should instantiate a new game' do
    sign_in users :one
    post games_play_url params: { file: file_fixture('good_game.txt') }
    assert_not_nil controller.view_assigns['game']
  end

  test 'should post random w/o params' do
    sign_in users :one
    post games_random_url
    assert_response :ok
  end

  test 'random should instantiate a new game' do
    sign_in users :one
    post games_random_url
    assert_not_nil controller.view_assigns['game']
  end

  test 'should not post start w/o game params' do
    sign_in users :one
    post games_start_url
    assert_redirected_to games_new_path
  end

  test 'should post start w/ game params' do
    sign_in users :one
    post games_start_url params: { game: '{}' }
    assert_response :ok
  end

  test 'post should respond with game and grid html' do
    sign_in users :one
    post games_start_url params: { game: '{}' }
    res = @response.parsed_body
    assert_not_nil res['game']
    assert_not_nil res['grid']
  end
end
