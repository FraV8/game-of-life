require 'test_helper'

class GameTest < ActiveSupport::TestCase
  test 'should refine uploaded data' do
    upload = Upload.new file_fixture('small_game.txt')
    game = Game.new upload
    assert_equal game.generation, 2
    assert_equal game.rows, 3
    assert_equal game.cols, 4
    assert_equal game.grid, [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0]]
  end

  test 'should limit size to max 50' do
    upload = Upload.new file_fixture('huge_game.txt')
    game = Game.new upload
    assert_equal game.rows, 50
    assert_equal game.cols, 50
  end

  test 'should be able to load a random grid' do
    game = Game.new random: true
    assert game.generation
    assert game.rows
    assert game.cols
    assert_equal game.grid.size, game.rows
    assert_equal game.grid[0].size, game.cols
  end

  test 'should calculate the right next generation' do
    upload = Upload.new file_fixture('small_game.txt')
    game = Game.new upload
    game.next_generation
    assert_equal game.generation, 3
    assert_equal game.grid, [[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0]]
  end

  test 'should set game over if the next generation equals the previous' do
    upload = Upload.new file_fixture('small_game.txt')
    game = Game.new upload
    4.times do
      game.next_generation
    end
    assert_equal game.generation, 6
    assert_equal game.over, true
  end
end
