require 'test_helper'

class UploadTest < ActiveSupport::TestCase
  test 'should not be valid w/o file' do
    upload = Upload.new ''
    assert_not upload.valid?
  end

  test 'should not be valid w/ bad file' do
    upload = Upload.new file_fixture('bad_game.txt')
    assert_not upload.valid?
  end

  test 'should be valid w/ good file' do
    upload = Upload.new file_fixture('good_game.txt')
    assert upload.valid?
  end

  test 'should parse file and collect required data' do
    upload = Upload.new file_fixture('good_game.txt')
    assert_equal upload.generation, '8'
    assert_equal upload.rows, '8'
    assert_equal upload.cols, '6'
    assert_equal upload.cells, [[0, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0], [0, 1, 0, 1, 0, 0], [0, 1, 1, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
  end
end
