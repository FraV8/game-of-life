Rails.application.routes.draw do
  devise_for :users

  namespace :games, path: '/' do
    get :new
    get :play, to: :new
    post :play
    post :random
    post :start
  end

  root to: redirect('/new')
  get '*path', to: redirect('/new')
end
